
var messenger = (function(){
    var count = 0;
    var firstName = null;
    var secondName = null;

    function setFirstName(name) {
        firstName = "Mr. " + name;
    }

    function setSecondName(surname) {
        secondName = surname;
    }

    function getMessageText() {
        var greetingMessage = "Welcome " + firstName + " ";
        greetingMessage += secondName + ".Glad to see you ";
        return greetingMessage;
    }

   function resetCount() {
        count = 0;
        console.log("Count is 0");
   }

    return {
        init: function (firstName, secondName) {
            setFirstName(firstName);
            setSecondName(secondName);
        },
        sayHello: function() {
            console.log(getMessageText() + "Count " +(count++));
        }
    }
}());

messenger.init("John", "Smith");
messenger.sayHello();


