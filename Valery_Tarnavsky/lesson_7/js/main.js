var first = document.querySelector('#first-line');
var second = document.querySelector('#second-line');
var third = document.querySelector('#third-line');
var transformedArray = transformArray(data);

function capitalizeFirstLetter(s) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}

function cutString (s){
    return s.slice(0, 15) + "...";
}

function formatDate(date){
    var tmpDate = new Date(date);
    var fixDate = {
        year    : tmpDate.getFullYear(),
        month   : tmpDate.getMonth() + 1,
        day     : tmpDate.getDate(),
        hours   : tmpDate.getHours(),
        minutes : tmpDate.getMinutes()
    };
    for (var k in fixDate) {
        fixDate[k] = fixDate[k] < 10  ? ('0'+ fixDate[k]) : fixDate[k];
    }
    return fixDate.year + "/" + fixDate.month + "/" + fixDate.day + " " +  fixDate.hours + ":" + fixDate.minutes;
}

function transformArray (arr){
    return arr.map(function(item) {
        return {
            url        : "http://" + item.url,
            name       : capitalizeFirstLetter(item.name),
            params     : item.params.status + "=>" + item.params.progress,
            description: cutString(item.description),
            date       : formatDate(item.date)
        }
    });
}

function print(tmp, elementId){
    elementId.innerHTML += tmp;
}

function replaceItems(item){
    var itemTemplate = '<div class="col-sm-3 col-xs-6">\
                            <img src="$url" alt="$name" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">$name</div>\
                                <div class="text-muted">$description</div>\
                                <div class="text-muted">$params</div>\
                                <div class="text-muted">$date</div>\
                            </div>\
                        </div>';
    var result = "";
    result += itemTemplate
        .replace(/\$name/gi,item.name)
        .replace("$url", item.url)
        .replace("$params", item.params)
        .replace("$description", item.description)
        .replace("$date", item.date);
    return result;
}

function interpolateItems(item){
    var itemTemplate = `<div class="col-sm-3 col-xs-6">\
        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
            <div class="info-wrapper">\
                <div class="text-muted">${item.name}</div>\
                <div class="text-muted">${item.description}</div>\
                <div class="text-muted">${item.params}</div>\
                <div class="text-muted">${item.date}</div>\
            </div>\
        </div>`
    var result = "";
        result += itemTemplate;
    return result;
}

function createItems(items){
    var grid = document.createElement("div");
    grid.className = "col-sm-3 col-xs-6";

    var thumbnail = document.createElement("img");
    thumbnail.setAttribute("src", items.url);
    thumbnail.setAttribute("alt", items.name);
    thumbnail.className = "img-thumbnail";

    var info = document.createElement("div");
    info.className = "info-wrapper";

    var innerName = document.createElement("div");
    innerName.className = "text-muted";
    innerName.innerHTML =  items.name;
    innerParams = innerName.cloneNode(true);
    innerParams.innerHTML = items.params;
    innerDescription = innerName.cloneNode(true);
    innerDescription.innerHTML = items.description;
    innerDate = innerName.cloneNode(true);
    innerDate.innerHTML = items.date;

    info.appendChild(innerName);
    info.appendChild(innerDescription);
    info.appendChild(innerParams);
    info.appendChild(innerDate);
    grid.appendChild(thumbnail);
    grid.appendChild(info);

    return grid;

}

function buildGallery(arr){
    arr.forEach(function(item, index) {
        if (index <= 2){
        print(replaceItems(item), first);
        }else if(index >= 3 && index <= 5){
        print(interpolateItems(item), second);
        }else if (index >= 6 && index <= 8){
        print(createItems(item).outerHTML, third);
        }
    });
}

buildGallery(transformedArray);









