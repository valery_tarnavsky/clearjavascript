﻿var resultElement  = document.getElementById("result");

function getRndNumber(){
    return Math.floor((Math.random() * 6) + 1);
}

function print (text) {
    resultElement.innerHTML += text;
}

function isNumbersEqual (number1, number2) {
    if (number1 == number2){
        print("Выпал дубль. Число: " + number1 + "<br />");
    }
}

function isBigDifference (number1, number2) {
    if (number1 < 3 && number2 > 4){
        var subtraction = number2 - number1;
        print("Большой разброс между костями. Разница составляет: " + subtraction + "<br />");
    }
}

function getGameResult(total){
    var result = total > 100 ? "Победа! Cумма Ваших очков: " + total : "Вы проиграли, сумма Ваших очков: " + total;
    return result;
}

function run (){
    var total = 0;
    for (var i = 1; i <= 15; i++){
        if (i == 8 || i == 13){continue;}
        var first  = getRndNumber();
        var second = getRndNumber();
        print('<span class="count">' + i + '</span><br />Первая кость: <span>' + first +'</span>. Вторая кость: <span>' + second + '</span><br />');
        isNumbersEqual(first, second);
        isBigDifference(first, second);
        total += first + second;
    }
    print('<div class="total">' + getGameResult(total) + '</div>');
}
run();

